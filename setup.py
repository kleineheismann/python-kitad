#!/usr/bin/env python
import os
import re
from setuptools import setup


def get_version():
    version = '0.dev0'
    expr = re.compile(r'^PROGRAM_VERSION\s*=\s*\'(.*)\'')
    path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'kitad.py')
    with open(path) as file:
        for line in file:
            match = expr.match(line)
            if match:
                version = match.group(1)
    return version


def get_long_description():
    path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'README.rst')
    with open(path) as file:
        return file.read()


def get_url():
    url = 'file://' + os.path.dirname(os.path.abspath(__file__))
    expr = re.compile(r'^PROGRAM_URL\s*=\s*\'(.*)\'')
    path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'kitad.py')
    with open(path) as file:
        for line in file:
            match = expr.match(line)
            if match:
                url = match.group(1)
    return url


setup(
    name='kitad',
    version=get_version(),
    description='kitad.py is a python script to access the KIT Active Directory via LDAP.',
    long_description=get_long_description(),
    long_description_content_type='text/x-rst',
    author='Jens Kleineheismann',
    author_email='kleineheismann@kit.edu',
    url=get_url(),
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Programming Language :: Python :: 3',
        'Operating System :: POSIX',
        'Environment :: Console',
        'License :: CC0 1.0 Universal (CC0 1.0) Public Domain Dedication'
    ],
    scripts=['kitad.py'],
    install_requires=[
        'python-ldap',
    ],
    python_requires='>=3',
)
