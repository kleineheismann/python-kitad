import posix

from .basic_test_cases import ProgramTestCase


class Usage(ProgramTestCase):
    def test_builtin_help(self):
        self.runProgram(argv=['groups', '-h'])
        self.assertOutputStartsWith('usage: ')
        self.assertExitVal(posix.EX_OK, 'Program did not return posix.EX_OK')
