# coding: utf8

# Attributes of a real KIT user with unique surname
TEST_USERNAME = u'zy8373'
TEST_GIVENNAME = u'Jens'
TEST_SURNAME = u'Kleineheismann'
TEST_REALNAME = u'{gn} {sn}'.format(gn=TEST_GIVENNAME, sn=TEST_SURNAME)
TEST_MAILADDR = u'kleineheismann@kit.edu'
TEST_OE = u'IISM'
TEST_UID = u'35257'

# A surname, that matches multiple KIT User
TEST_AMBIGUOUS_SURNAME = u'Müller'

# Testing realname patterns
# Here we need a realname that match some users
# and match more users if '*' is appended to it.
TEST_REALNAME_FOR_PATTERN = u'klein'

# A syntactically valid username, that does not exist
TEST_VOID_USERNAME = u'ugly'
