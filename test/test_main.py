import posix
import sys
import unittest
from io import StringIO

from kitad import main


class MainTestCase(unittest.TestCase):
    def test_main_without_arguments(self):
        orig_stdout = sys.stdout
        sys.stdout = StringIO()

        try:
            main(argv=[])
            result = None
        except SystemExit as e:
            result = e.code
        finally:
            output = sys.stdout.getvalue()
            sys.stdout.close()
            sys.stdout = orig_stdout

        is_usage = output.startswith('usage:')
        self.assertTrue(is_usage, 'main() does not produce usage text')
        self.assertEqual(posix.EX_OK, result, 'main() does not exit with posix.EX_OK')
