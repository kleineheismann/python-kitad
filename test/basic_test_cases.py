import logging
import shlex
import sys
import unittest
from io import StringIO

from kitad import Program, logger


class ProgramTestCase(unittest.TestCase):
    @staticmethod
    def argv_split(text):
        return shlex.split(text)

    def setUp(self):
        self._program = Program()
        self.exitval = None

        self._real_stdout, self._real_stderr, self._log_handler, _ = self.startOutputCapture()

    def tearDown(self):
        self.stopOutputCapture(self._real_stdout, self._real_stderr, self._log_handler)

    def startOutputCapture(self):
        orig_stdout = sys.stdout
        sys.stdout = StringIO()
        self.stdout_output = None
        orig_stderr = sys.stderr
        sys.stderr = StringIO()
        self.stderr_output = None
        self._logIO = StringIO()
        log_handler = logging.StreamHandler(self._logIO)
        log_handler.setFormatter(logging.Formatter('%(levelname)s: %(message)s'))
        logger.addHandler(log_handler)
        orig_propagate = logger.propagate
        logger.propagate = False
        self.log_output = None
        return orig_stdout, orig_stderr, log_handler, orig_propagate

    def stopOutputCapture(self, stdout, stderr, log_handler=None, orig_propagate=True):
        sys.stdout.close()
        sys.stdout = stdout
        sys.stderr.close()
        sys.stderr = stderr
        if log_handler:
            logger.removeHandler(log_handler)
        else:
            logger.removeHandler(logger.handlers[0])
        logger.propagate = orig_propagate

    def clearOutput(self):
        self.stopOutputCapture(self._real_stdout, self._real_stderr, self._log_handler)
        self._real_stdout, self._real_stderr, self._log_handler, _ = self.startOutputCapture()

    def runProgram(self, *args, **kwargs):
        try:
            exitval = self._program(*args, **kwargs)
        except SystemExit as e:
            exitval = e.code
        finally:
            self.stdout_output = sys.stdout.getvalue()
            self.stderr_output = sys.stderr.getvalue()
            self.log_output = self._logIO.getvalue()

        self.exitval = exitval
        return exitval

    def assertExitVal(self, exitval, msg=None):
        if msg is None:
            msg = 'Program did not return \'{val}\''.format(val=exitval)

        self.assertEqual(exitval, self.exitval, msg)

    def assertOutputEmpty(self, msg=None, channel='stdout'):
        if channel == 'log':
            output = self.log_output
        elif channel == 'stderr':
            output = self.stderr_output
        else:
            output = self.stdout_output

        if msg is None:
            msg = 'Output ({channel}) is not empty\nCaptured Output:\n{output}'.format(channel=channel,
                                                                                       output=output)

        self.assertEqual('', output, msg)

    def assertOutput(self, text, msg=None, channel='stdout'):
        if channel == 'log':
            output = self.log_output
        elif channel == 'stderr':
            output = self.stderr_output
        else:
            output = self.stdout_output

        if msg is None:
            msg = ('Output ({channel}) is not \'{text}\'\n'
                   'Captured Output:\n{output}').format(channel=channel,
                                                        text=text.replace('\n', '\\n'),
                                                        output=output)

        self.assertEqual(text, output, msg)

    def assertOutputEndsWith(self, text, msg=None, channel='stdout'):
        if channel == 'log':
            output = self.log_output
        elif channel == 'stderr':
            output = self.stderr_output
        else:
            output = self.stdout_output

        if msg is None:
            msg = ('Output ({channel}) did not end with \'{text}\'\n'
                   'Captured Output:\n{output}').format(channel=channel,
                                                        text=text.replace('\n', '\\n'),
                                                        output=output)

        self.assertTrue(output.endswith(text), msg)

    def assertOutputStartsWith(self, text, msg=None, channel='stdout'):
        if channel == 'log':
            output = self.log_output
        elif channel == 'stderr':
            output = self.stderr_output
        else:
            output = self.stdout_output

        if msg is None:
            msg = ('Output ({channel}) did not start with \'{text}\'\n'
                   'Captured Output:\n{output}').format(channel=channel,
                                                        text=text.replace('\n', '\\n'),
                                                        output=output)

        self.assertTrue(output.startswith(text), msg)
