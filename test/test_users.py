import posix
import unicodedata

from .basic_test_cases import ProgramTestCase
from .config import (TEST_USERNAME,
                     TEST_SURNAME,
                     TEST_REALNAME,
                     TEST_MAILADDR,
                     TEST_OE,
                     TEST_UID,
                     TEST_AMBIGUOUS_SURNAME,
                     TEST_REALNAME_FOR_PATTERN,
                     TEST_VOID_USERNAME)

from kitad import RECORD_DIVIDER, PSEUDO_SYMBOL


class Usage(ProgramTestCase):
    def test_too_few_arguments(self):
        self.runProgram(argv=['users'])
        self.assertOutputEndsWith('error: the following arguments are required: USER, ...\n', channel='stderr')
        self.assertExitVal(2)

    def test_builtin_help(self):
        self.runProgram(argv=['users', '-h'])
        self.assertOutputStartsWith('usage: ')
        self.assertExitVal(posix.EX_OK, 'Program did not return posix.EX_OK')


class ArgumentAutoDetect(ProgramTestCase):
    def test_username(self):
        argv_input = TEST_USERNAME
        expected_log_output = 'INFO: Consider \'{input}\' as username\n'.format(input=argv_input)
        expected_output = '{user} - {name} ({oe}) <{mail}>\n'.format(user=TEST_USERNAME,
                                                                     name=TEST_REALNAME,
                                                                     oe=TEST_OE,
                                                                     mail=TEST_MAILADDR)

        argv = ['-v', 'users'] + self.argv_split(argv_input)
        self.runProgram(argv=argv)

        self.assertOutput(expected_output)
        self.assertOutput(expected_log_output, channel='log')
        self.assertExitVal(posix.EX_OK, 'Program did not return posix.EX_OK')

    def test_uid(self):
        argv_input = TEST_UID
        expected_log_output = 'INFO: Consider \'{input}\' as uid\n'.format(input=argv_input)
        expected_output = '{user} - {name} ({oe}) <{mail}>\n'.format(user=TEST_USERNAME,
                                                                     name=TEST_REALNAME,
                                                                     oe=TEST_OE,
                                                                     mail=TEST_MAILADDR)

        argv = ['-v', 'users'] + self.argv_split(argv_input)
        self.runProgram(argv=argv)

        self.assertOutput(expected_output)
        self.assertOutput(expected_log_output, channel='log')
        self.assertExitVal(posix.EX_OK, 'Program did not return posix.EX_OK')

    def test_mailaddr(self):
        argv_input = TEST_MAILADDR
        expected_log_output = 'INFO: Consider \'{input}\' as mail\n'.format(input=argv_input)
        expected_output = '{user} - {name} ({oe}) <{mail}>\n'.format(user=TEST_USERNAME,
                                                                     name=TEST_REALNAME,
                                                                     oe=TEST_OE,
                                                                     mail=TEST_MAILADDR)

        argv = ['-v', 'users'] + self.argv_split(argv_input)
        self.runProgram(argv=argv)

        self.assertOutput(expected_output)
        self.assertOutput(expected_log_output, channel='log')
        self.assertExitVal(posix.EX_OK, 'Program did not return posix.EX_OK')

    def test_realname(self):
        argv_input = TEST_REALNAME
        expected_log_output = 'INFO: Consider \'{input}\' as realname\n'.format(input=argv_input)
        expected_output = '{user} - {name} ({oe}) <{mail}>\n'.format(user=TEST_USERNAME,
                                                                     name=TEST_REALNAME,
                                                                     oe=TEST_OE,
                                                                     mail=TEST_MAILADDR)

        argv = ['-v', 'users'] + self.argv_split(argv_input)
        self.runProgram(argv=argv)

        self.assertOutput(expected_output)
        self.assertOutput(expected_log_output, channel='log')
        self.assertExitVal(posix.EX_OK, 'Program did not return posix.EX_OK')

    def test_surname(self):
        argv_input = TEST_SURNAME
        expected_log_output = 'INFO: Consider \'{input}\' as realname\n'.format(input=argv_input)
        expected_output = '{user} - {name} ({oe}) <{mail}>\n'.format(user=TEST_USERNAME,
                                                                     name=TEST_REALNAME,
                                                                     oe=TEST_OE,
                                                                     mail=TEST_MAILADDR)

        argv = ['-v', 'users'] + self.argv_split(argv_input)
        self.runProgram(argv=argv)

        self.assertOutput(expected_output)
        self.assertOutput(expected_log_output, channel='log')
        self.assertExitVal(posix.EX_OK, 'Program did not return posix.EX_OK')

    def test_default_verbosity(self):
        argv_input = TEST_USERNAME
        expected_output = '{user} - {name} ({oe}) <{mail}>\n'.format(user=TEST_USERNAME,
                                                                     name=TEST_REALNAME,
                                                                     oe=TEST_OE,
                                                                     mail=TEST_MAILADDR)

        argv = ['users'] + self.argv_split(argv_input)
        self.runProgram(argv=argv)

        self.assertOutput(expected_output)
        self.assertOutputEmpty(channel='log')


class InputOptions(ProgramTestCase):
    def test_username(self):
        argv_input = '-U {user}'.format(user=TEST_USERNAME)
        expected_output = '{name}\n'.format(name=TEST_REALNAME)

        argv = ['-q', 'users'] + self.argv_split(argv_input)
        self.runProgram(argv=argv)

        self.assertOutput(expected_output)
        self.assertExitVal(posix.EX_OK, 'Program did not return posix.EX_OK')

    def test_uid(self):
        argv_input = '-I {uid}'.format(uid=TEST_UID)
        expected_output = '{user}\n'.format(user=TEST_USERNAME)

        argv = ['-q', 'users'] + self.argv_split(argv_input)
        self.runProgram(argv=argv)

        self.assertOutput(expected_output)
        self.assertExitVal(posix.EX_OK, 'Program did not return posix.EX_OK')

    def test_mailaddr(self):
        argv_input = '-M {mail}'.format(mail=TEST_MAILADDR)
        expected_output = '{user}\n'.format(user=TEST_USERNAME)

        argv = ['-q', 'users'] + self.argv_split(argv_input)
        self.runProgram(argv=argv)

        self.assertOutput(expected_output)
        self.assertExitVal(posix.EX_OK, 'Program did not return posix.EX_OK')

    def test_realname(self):
        argv_input = '-R {name}'.format(name=TEST_REALNAME)
        expected_output = '{user}\n'.format(user=TEST_USERNAME)

        argv = ['-q', 'users'] + self.argv_split(argv_input)
        self.runProgram(argv=argv)

        self.assertOutput(expected_output)
        self.assertExitVal(posix.EX_OK, 'Program did not return posix.EX_OK')

    def test_some_attribute(self):
        argv_input = '-A samaccountname {user}'.format(user=TEST_USERNAME)
        expected_output = '{user}\n'.format(user=TEST_USERNAME)

        argv = ['-q', 'users'] + self.argv_split(argv_input)
        self.runProgram(argv=argv)

        self.assertOutput(expected_output)
        self.assertExitVal(posix.EX_OK, 'Program did not return posix.EX_OK')


class OutputOptions(ProgramTestCase):
    def test_realname(self):
        argv_input = '-n {user}'.format(user=TEST_USERNAME)
        expected_output = '{name}\n'.format(name=TEST_REALNAME)

        argv = ['users'] + self.argv_split(argv_input)
        self.runProgram(argv=argv)

        self.assertOutput(expected_output)
        self.assertExitVal(posix.EX_OK, 'Program did not return posix.EX_OK')

    def test_username(self):
        argv_input = '-u {uid}'.format(uid=TEST_UID)
        expected_output = '{user}\n'.format(user=TEST_USERNAME)

        argv = ['users'] + self.argv_split(argv_input)
        self.runProgram(argv=argv)

        self.assertOutput(expected_output)
        self.assertExitVal(posix.EX_OK, 'Program did not return posix.EX_OK')

    def test_some_attribute(self):
        argv_input = '-o mail {user}'.format(user=TEST_USERNAME)
        expected_output = '{mail}\n'.format(mail=TEST_MAILADDR)

        argv = ['users'] + self.argv_split(argv_input)
        self.runProgram(argv=argv)

        self.assertOutput(expected_output)
        self.assertExitVal(posix.EX_OK, 'Program did not return posix.EX_OK')

    def test_username_prefix_option(self):
        argv_input = '-i -o mail {user}'.format(user=TEST_USERNAME)
        expected_output = '{user}: {mail}\n'.format(user=TEST_USERNAME,
                                                    mail=TEST_MAILADDR)

        argv = ['users'] + self.argv_split(argv_input)
        self.runProgram(argv=argv)

        self.assertOutput(expected_output)
        self.assertExitVal(posix.EX_OK, 'Program did not return posix.EX_OK')

    def test_username_attribute_prefix_options(self):
        argv_input = '-i -k -o mail {user}'.format(user=TEST_USERNAME)
        expected_output = '{user}: mail: {mail}\n'.format(user=TEST_USERNAME,
                                                          mail=TEST_MAILADDR)

        argv = ['users'] + self.argv_split(argv_input)
        self.runProgram(argv=argv)

        self.assertOutput(expected_output)
        self.assertExitVal(posix.EX_OK, 'Program did not return posix.EX_OK')

    def test_attribute_username_prefix_options(self):
        argv_input = '-ki -o mail {user}'.format(user=TEST_USERNAME)
        expected_output = 'mail: {user}: {mail}\n'.format(user=TEST_USERNAME,
                                                          mail=TEST_MAILADDR)

        argv = ['users'] + self.argv_split(argv_input)
        self.runProgram(argv=argv)

        self.assertOutput(expected_output)
        self.assertExitVal(posix.EX_OK, 'Program did not return posix.EX_OK')

    def test_attribute_prefix_option(self):
        argv_input = '-k -o mail {user}'.format(user=TEST_USERNAME)
        expected_output = 'mail: {mail}\n'.format(mail=TEST_MAILADDR)

        argv = ['users'] + self.argv_split(argv_input)
        self.runProgram(argv=argv)

        self.assertOutput(expected_output)
        self.assertExitVal(posix.EX_OK, 'Program did not return posix.EX_OK')

    def test_attribute_list(self):
        argv_input = '-o uid,mail,realname {user}'.format(user=TEST_USERNAME)
        expected_output = ('mail: {mail}\n'
                           'realname: {name}\n'
                           'uid: {uid}\n').format(mail=TEST_MAILADDR,
                                                  name=TEST_REALNAME,
                                                  uid=TEST_UID)

        argv = ['users'] + self.argv_split(argv_input)
        self.runProgram(argv=argv)

        self.assertOutput(expected_output)
        self.assertExitVal(posix.EX_OK, 'Program did not return posix.EX_OK')

    def test_multiple_attribute_options(self):
        argv_input = '-o uid -o mail,realname {user}'.format(user=TEST_USERNAME)
        expected_output = ('mail: {mail}\n'
                           'realname: {name}\n'
                           'uid: {uid}\n').format(mail=TEST_MAILADDR,
                                                  name=TEST_REALNAME,
                                                  uid=TEST_UID)

        argv = ['users'] + self.argv_split(argv_input)
        self.runProgram(argv=argv)

        self.assertOutput(expected_output)
        self.assertExitVal(posix.EX_OK, 'Program did not return posix.EX_OK')

    def test_all_attributes(self):
        argv_input = '-o all {user}'.format(user=TEST_USERNAME)
        expected_output_start = '%dn: CN='
        expected_lines = [
            '{psym}realname: {name}\n'.format(psym=PSEUDO_SYMBOL, name=TEST_REALNAME),
            '{psym}username: {user}\n'.format(psym=PSEUDO_SYMBOL, user=TEST_USERNAME),
            'mail: {mail}\n'.format(mail=TEST_MAILADDR),
        ]

        argv = ['users'] + self.argv_split(argv_input)
        self.runProgram(argv=argv)

        missing_line = None
        for expected_line in expected_lines:
            if expected_line not in self.stdout_output:
                missing_line = expected_line
                break

        if missing_line:
            missing_line = missing_line.replace('\n', '\\n')

        self.assertOutputStartsWith(expected_output_start)
        self.assertIsNone(missing_line,
                          'Output did not contain \'{line}\''.format(line=missing_line))
        self.assertExitVal(posix.EX_OK, 'Program did not return posix.EX_OK')


class ShowUser(ProgramTestCase):
    def test_ambiguous(self):
        argv_input = TEST_AMBIGUOUS_SURNAME
        expected_log_output = 'ERROR: \'{name}\' is ambiguous\n'.format(name=TEST_AMBIGUOUS_SURNAME)

        argv = ['users', '-s'] + self.argv_split(argv_input)
        self.runProgram(argv=argv)

        self.assertOutput(expected_log_output, channel='log')
        self.assertOutputEmpty()
        self.assertExitVal(posix.EX_DATAERR, 'Program did not return posix.EX_DATAERR')


class ListMultipleUsers(ProgramTestCase):
    def test_implicit_username_prefix(self):
        argv_input = '{name}'.format(name=TEST_AMBIGUOUS_SURNAME)
        log_regexp = '^WARNING: Found [0-9]+ user\n'
        output_line_regexp = '^.*: .* - .* {name}'.format(name=TEST_AMBIGUOUS_SURNAME)

        argv = ['users', '-l'] + self.argv_split(argv_input)
        self.runProgram(argv=argv)

        log_output = self.log_output
        output = self.stdout_output

        for line in output.splitlines():
            line_normalized = unicodedata.normalize('NFKD', line).encode('ascii', 'ignore')
            regexp_normalized = unicodedata.normalize('NFKD', output_line_regexp).encode('ascii', 'ignore')
            self.assertRegex(line_normalized, regexp_normalized)

        self.assertRegex(log_output, log_regexp,
                         'Log output does not match {regexp}'.format(regexp=log_regexp))
        self.assertExitVal(posix.EX_OK, 'Program did not return posix.EX_OK')

    def test_suppress_username_prefix(self):
        argv_input = '-o sn {name}'.format(name=TEST_AMBIGUOUS_SURNAME)
        output_line_regexp = '^{name}'.format(name=TEST_AMBIGUOUS_SURNAME)

        argv = ['-q', 'users', '-l'] + self.argv_split(argv_input)
        self.runProgram(argv=argv)

        output = self.stdout_output

        for line in output.splitlines():
            line_normalized = unicodedata.normalize('NFKD', line).encode('ascii', 'ignore')
            regexp_normalized = unicodedata.normalize('NFKD', output_line_regexp).encode('ascii', 'ignore')
            self.assertRegex(line_normalized, regexp_normalized)

        self.assertOutputEmpty(channel='log')
        self.assertExitVal(posix.EX_OK, 'Program did not return posix.EX_OK')

    def test_record_seperator(self):
        argv_input = '-o sn,username {name}'.format(name=TEST_AMBIGUOUS_SURNAME)
        log_regexp = '^WARNING: Found [0-9]+ user\n'
        record_regexp = ('^.*: sn: {name}\n'
                         '.*: username: .*\n$').format(name=TEST_AMBIGUOUS_SURNAME)

        argv = ['users', '-l'] + self.argv_split(argv_input)
        self.runProgram(argv=argv)

        log_output = self.log_output
        output = self.stdout_output

        regexp_normalized = unicodedata.normalize('NFKD', record_regexp).encode('ascii', 'ignore').decode()

        in_record = True
        record = ''
        for line in output.splitlines():
            if line == RECORD_DIVIDER:
                in_record = False
            else:
                username, attribute, value = line.split(': ', 2)
                value_normalized = unicodedata.normalize('NFKD', value).encode('ascii', 'ignore').decode()
                record += '{user}: {attr}: {val}\n'.format(user=username, attr=attribute, val=value_normalized)

            if not in_record:
                self.assertRegex(record, regexp_normalized)
                record = ''
                in_record = True

        self.assertRegex(record, regexp_normalized)

        self.assertRegex(log_output, log_regexp,
                         'Log output does not match {regexp}'.format(regexp=log_regexp))
        self.assertExitVal(posix.EX_OK, 'Program did not return posix.EX_OK')


class ListGroups(ProgramTestCase):
    def test_direct(self):
        argv_input = '{user}'.format(user=TEST_USERNAME)
        expected_output_line = '{oe}-Users-IDM\n'.format(oe=TEST_OE)

        argv = ['users', '-g'] + self.argv_split(argv_input)
        self.runProgram(argv=argv)

        self.assertIn(expected_output_line, self.stdout_output)
        self.assertExitVal(posix.EX_OK, 'Program did not return posix.EX_OK')

    def test_recursive(self):
        argv_input = '{user}'.format(user=TEST_USERNAME)
        expected_output_line = '{oe}-Users\n'.format(oe=TEST_OE)

        argv = ['users', '-gr'] + self.argv_split(argv_input)
        self.runProgram(argv=argv)

        self.assertIn(expected_output_line, self.stdout_output)
        self.assertExitVal(posix.EX_OK, 'Program did not return posix.EX_OK')


class ExceptionalArguments(ProgramTestCase):
    def test_void_username(self):
        argv_input = TEST_VOID_USERNAME
        expected_log_output = 'ERROR: No user found\n'

        argv = ['users'] + self.argv_split(argv_input)
        self.runProgram(argv=argv)

        self.assertOutput(expected_log_output, channel='log')
        self.assertExitVal(posix.EX_NOUSER, 'Program did not return posix.EX_NOUSER')

    def test_asterisk_in_argument(self):
        argv_input_wo_pattern = '-o sn -R {name}'.format(name=TEST_REALNAME_FOR_PATTERN)
        argv_input_w_pattern = '-o sn -R "{name}*"'.format(name=TEST_REALNAME_FOR_PATTERN)
        log_regexp = '^WARNING: Found [0-9]+ user\n'

        argv = ['users'] + self.argv_split(argv_input_wo_pattern)
        self.runProgram(argv=argv)

        matches_wo_pattern = len(self.stdout_output.splitlines())
        self.clearOutput()

        argv = ['users'] + self.argv_split(argv_input_w_pattern)
        self.runProgram(argv=argv)

        matches_w_pattern = len(self.stdout_output.splitlines())
        log_output = self.log_output

        self.assertGreater(matches_w_pattern, matches_wo_pattern)
        self.assertRegex(log_output, log_regexp,
                         'Log output does not match {regexp}'.format(regexp=log_regexp))
        self.assertExitVal(posix.EX_OK, 'Program did not return posix.EX_OK')
