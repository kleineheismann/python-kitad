ABOUT
=====
kitad.py is a python script to access the KIT Active Directory via LDAP.

For example kitad.py can

- resolve KIT usernames into realnames and vice versa
- show attributes of users like mail address, phone number, etc.
- show group membership
- list user groups
- list AD Organizational Units (OUs)
- list hosts within OUs
- list attributes of hosts like service principle names
- read/write key-value pairs from/into hosts comment attribute


REQUIREMENTS / PRELIMINARIES
============================
- Python >= 3

- python-ldap (https://www.python-ldap.org)

  - python-ldap must be build with support for TLS, SASL and KerberosV

  - the SASL library must support GSSAPI (see TROUBLESHOOTING)

  - the openldap library must be configured properly (see TROUBLESHOOTING)


INSTALLATION
============
Just put the kitad.py file somewhere in your PATH.

``# cp kitad.py /usr/local/bin``

or

``$ cp kitad.py ~/.local/bin``

You also want to modify some hardcoded defaults within the file.


USAGE
=====

``$ kitad.py -h``

For convenience it is recommended to acquire a KerberosV Ticket
Granting Ticket, so you can access the Active Directory without
entering your password every time.

User and host attributes
------------------------
The -o option of the users and hosts sub commands can be used to specify
which attributes should be shown.
In addition of the common Active Directory LDAP attributes some pseudo
attributes can be selected. To see all possible attributes of an entry,
use ``-o all``. The pseudo attributes within the resulting list will be
tagged with a prefix symbol (either % or #).
If you specify pseudo attributes to the -o option, the symbol prefix is
optional.

Examples:
---------
::

  $ kitad.py users Holger Hanselka
  $ kitad.py users -o all ab1234
  $ kitad.py users -o phone,mail -o oe Muster*
  $ kitad.py groups IOE-*
  $ kitad.py hosts /KIT/Misc/IOE/Computers
  $ kitad.py hosts -m ioe-pc1 ether ab:cd:ef:12:34:56
  $ kitad.py hosts -o ether /KIT/Misc/IOE/Computers/lab

IOE: *Institute of Examples*


TROUBLESHOOTING
===============
python-ldap
-----------
The ldap package provide two constants to signal the support for SASL and TLS. Both must equal to 1.

::

  $ python
  >>> import ldap
  >>> ldap.SASL_AVAIL
  1
  >>> ldap.TLS_AVAIL
  1

KerberosV
---------
To ensure, that KerberosV work, acquire a TGT via ``kinit`` command
and list your cached tickets via ``klist`` command.

GSSAPI
------
The SASL library consist of several modules to supports different mechanisms.
Thus the library is often distributed in multiple packages.
Make sure, that the GSSAPI module of the SASL library is installed.

On RedHat/Fedora based distributions the required package is called
``cyrus-sasl-gssapi``

On Debian/Ubuntu based distributions the required package is called
``libsasl2-modules-gssapi-mit`` or ``libsasl2-modules-gssapi-heimdal``

OpenLDAP
--------
Run ``ldapsearch -H ldaps://kit-dc-08.kit.edu -s base -b DC=kit,DC=edu``
to check, if openldap work.

On some installations it is necessary to limit the Security Strength Factor
(SSF) in order to talk to Microsoft Active Directory LDAP servers.
Therefor the openldap configuration (``/etc/openldap/ldap.conf`` on most linux
systems) must contain the following line:

::

  SASL_SECPROPS maxssf=128

Some installations also have to be told where to find trusted X.509 Certificates:

::

  TLS_CACERT /etc/ssl/certs/ca-bundle.crt


LICENCE
=======
Author: Jens Kleineheismann <kleineheismann@kit.edu>

Permission to use, copy, modify, and/or distribute this software
for any purpose with or without fee is hereby granted.
