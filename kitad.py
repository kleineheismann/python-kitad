#!/usr/bin/env python
import argparse
import base64
import datetime
import getpass
import logging
import os
import posix
import re
import sys
from collections import abc
import ldap
import ldap.sasl
import ldapurl

# Symbolic Constants
DNSEP = ','
OUSEP = '/'

PROGRAM_VERSION = '1.1.4'  # Note: This line will be searched vie regex by setup.py
PROGRAM_DESCRIPTION = 'Talk LDAPish with KIT Active Directory.'
PROGRAM_URL = 'https://git.scc.kit.edu/zy8373/python-kitad'  # Note: This line will be searched vie regex by setup.py
PASSWORD_PROMPT = 'Password: '

# Feature Config
RECORD_DIVIDER = '-' * 72
PSEUDO_SYMBOL = '%'
COMMENT_SYMBOL = '#'
COMMENT_FIELDSEP = '\''
ENABLE_HOST_COMMENT_MODIFY = True

# KIT Config
AD_BASE_DN = 'DC=kit,DC=edu'
SEARCH_BASE_DN = 'OU=KIT' + DNSEP + AD_BASE_DN
DEFAULT_LDAP_URI = 'ldap://kit-dc-22.kit.edu'

# OU Config
DEFAULT_GROUP_PATTERN = 'PRAESIDIUM-*'
DEFAULT_HOST_OU = '/KIT/Misc/PRAESIDIUM/Computers'

# User Config
# DEFAULT_SIMPLE_BIND_DN = 'CN=holger.hanselka,OU=IDM,OU=P1,OU=Staff,OU=KIT,DC=kit,DC=edu'
DEFAULT_SIMPLE_BIND_DN = None
DEFAULT_SIMPLE_BIND_PWD = """DO NOT STORE PASSWORDS WITHOUT PROPER ENCRYPTION! NO! NEVER! DON'T DO IT!"""


logging.basicConfig(format='%(levelname)s: %(message)s')
logger = logging.getLogger('__main__')


def usercert2pem(cert):
    linelength = 64
    asc = base64.b64encode(cert).decode()
    pem = '-----BEGIN CERTIFICATE-----'
    pem += '\n'
    for pos in range(0, len(asc), linelength):
        pem += asc[pos:pos + linelength] + '\n'
    pem += '-----END CERTIFICATE-----'
    return pem


def winticks2fstr(ticks, fmt='%Y-%m-%d %H:%M:%S'):
    win_epoch = datetime.datetime(1601, 1, 1)
    delta = datetime.timedelta(microseconds=ticks/10)
    timestamp = win_epoch + delta
    return timestamp.strftime(fmt)

class LdapApi:
    LDAP_URI = DEFAULT_LDAP_URI

    def __init__(self, uri=None, simple_bind=False, binddn=None, credential=None):
        if uri is None:
            uri = self.LDAP_URI
        self._uri = ldapurl.LDAPUrl(uri)

        self._simple_bind = simple_bind
        if simple_bind:
            self._auth_simple = (binddn, credential)
        else:
            self._auth_sasl = ldap.sasl.sasl({}, 'GSSAPI')

        self._ldap = ldap.initialize(self._uri.unparse())

    def bind(self):
        if self._simple_bind:
            self._ldap.simple_bind_s(*self._auth_simple)
        else:
            self._ldap.sasl_interactive_bind_s('', self._auth_sasl)

    def search(self, dn, scope, filterstr=None, **kwargs):
        if filterstr is None:
            filterstr = '(objectClass=*)'
        return self._ldap.search_ext_s(dn, scope, filterstr, **kwargs)

    def modify(self, dn, modlist):
        return self._ldap.modify_s(dn, modlist)


class BaseDict(abc.MutableMapping):
    def __init__(self, **kwargs):
        self.store = dict()
        self.update(**kwargs)

    def __getitem__(self, key):
        return self.store[self.__keytransform__(key)]

    def __setitem__(self, key, value):
        self.store[self.__keytransform__(key)] = value

    def __delitem__(self, key):
        del self.store[self.__keytransform__(key)]

    def __iter__(self):
        return iter(self.store)

    def __len__(self):
        return len(self.store)

    @staticmethod
    def __keytransform__(key):
        return key


class LdapRecord(BaseDict):
    BASE_DN = SEARCH_BASE_DN
    _DEFAULT_NAME_ATTRIBUTE = 'distinguishedname'
    _DEFAULT_REPR_ATTRIBUTE = 'distinguishedname'
    _PSEUDO_ATTRIBUTES = [PSEUDO_SYMBOL + 'dn']

    class NotLoadedException(Exception):
        pass

    class NotFoundException(Exception):
        pass

    class NotUniqueException(Exception):
        pass

    def __init__(self, ldap_api=None):
        if ldap_api is None:
            ldap_api = LdapApi()
        self._ldap_api = ldap_api
        self._default_name_attribute = self._DEFAULT_NAME_ATTRIBUTE
        self._default_repr_attribute = self._DEFAULT_REPR_ATTRIBUTE
        self._loaded = False
        super().__init__()

    def __getitem__(self, key):
        key = self.__keytransform__(key)
        if key == PSEUDO_SYMBOL + 'dn':
            return self['distinguishedname']
        return super().__getitem__(key)

    def __keytransform__(self, key):
        try:
            key = key.lower()
        except AttributeError:
            pass
        pseudo_key = PSEUDO_SYMBOL + key
        if pseudo_key in self._PSEUDO_ATTRIBUTES:
            key = pseudo_key
        return key

    def __str__(self):
        return self.name(self._default_repr_attribute)

    def search(self, dn=None, scope=None, filterstr=None, **kwargs):
        if dn is None:
            dn = self.BASE_DN
        if scope is None:
            scope = ldap.SCOPE_SUBTREE  # pylint: disable=no-member

        self._ldap_api.bind()
        try:
            results = self._ldap_api.search(dn, scope, filterstr)
        except ldap.NO_SUCH_OBJECT as exception:  # pylint: disable=no-member
            raise self.NotFoundException() from exception
        n = len(results)
        if n < 1:
            raise self.NotFoundException()
        if n > 1:
            raise self.NotUniqueException()
        self.load(results[0][1])
        return n

    def load(self, data):
        self.clear()
        for attr in self._PSEUDO_ATTRIBUTES:
            if attr not in data:
                data[attr] = []
        for attr, values in data.items():
            self[attr] = values
        self._loaded = True

    def modify(self, modlist):
        self._ldap_api.bind()
        return self._ldap_api.modify(self['distinguishedname'][0].decode(), modlist)

    def name(self, attribute=None):
        if not self._loaded:
            raise self.NotLoadedException()
        if attribute is None:
            attribute = self._default_name_attribute
        return self[attribute][0].decode()

    @classmethod
    def class_typename(cls, capitalize=True):
        name = cls.__name__.lower()
        name = name[len('Ldap'):]
        if capitalize:
            name = name.capitalize()
        return name


class LdapRecordSet(BaseDict):
    _RECORD_DEFAULT_REPR_ATTRIBUTE = None
    BASE_DN = SEARCH_BASE_DN
    RECORD_CLASS = LdapRecord

    class NotLoadedException(Exception):
        pass

    def __init__(self, ldap_api=None):
        if ldap_api is None:
            ldap_api = LdapApi()
        self._ldap_api = ldap_api
        self._loaded = False
        self._record_default_repr_attribute = self._RECORD_DEFAULT_REPR_ATTRIBUTE
        super().__init__()

    def names(self):
        return [r.name() for r in iter(self.values())]

    def search(self, dn=None, scope=None, filterstr=None, **kwargs):
        if dn is None:
            dn = self.BASE_DN
        if scope is None:
            scope = ldap.SCOPE_SUBTREE  # pylint: disable=no-member

        self._ldap_api.bind()
        results = self._ldap_api.search(dn, scope, filterstr)
        self.load(results)
        return len(results)

    def load(self, data):
        self.clear()
        for tup in data:
            dn = tup[0]
            if dn:
                recorddict = tup[1]
                record = self.RECORD_CLASS(ldap_api=self._ldap_api)
                record.load(data=recorddict)
                self[dn] = record
        self._loaded = True

    def list(self, attributes=None):
        if not self._loaded:
            raise self.NotLoadedException()

        if not attributes:
            attributes = [self._record_default_repr_attribute]
        elif isinstance(attributes, str):
            attributes = attributes.split(',')

        record_type = self.RECORD_CLASS.class_typename()
        tuples = []
        for dn in self:
            record = self[dn]
            record_name = record.name()
            record_tuples = []
            if 'all' in attributes:
                for attr in record:
                    try:
                        values = record[attr]
                    except KeyError:
                        values = []
                    for val in values:
                        tup = (attr, val)
                        record_tuples.append(tup)
            else:
                for attr in attributes:
                    if not attr:
                        values = [str(record)]
                    else:
                        try:
                            values = record[attr]
                        except KeyError as exception:
                            if len(attributes) > 1 or len(self) > 1:
                                logger.warning("%s '%s' has no attribute '%s'", record_type, record_name, attr)
                                values = []
                            else:
                                raise exception
                    for val in values:
                        tup = (attr, val)
                        record_tuples.append(tup)
            tup = (dn, record_name, record_tuples)
            tuples.append(tup)

        return tuples

    @classmethod
    def class_typename(cls, singular=True, capitalize=True):
        if singular:
            return cls.RECORD_CLASS.class_typename(capitalize)

        name = cls.__name__.lower()
        name = name[len('Ldap'):]
        name = name[:0 - len('Set')] + 's'
        if capitalize:
            name = name.capitalize()
        return name


class LdapUserIdentifierMixin:
    @staticmethod
    def _identifier_type_autodetect(identifier):
        if identifier.isdigit():
            idtype = 'uid'
        elif re.match(r'[a-z][a-z][0-9]{4}$', identifier):
            idtype = 'username'
        elif re.match(r'u[a-z]{3,4}$', identifier):
            idtype = 'username'
        elif re.match(r'[a-zA-Z0-9][a-zA-Z0-9.+_-]*@([a-zA-Z0-9]([a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])?\.)+[a-zA-Z]{2,}$',
                      identifier):
            idtype = 'mail'
        else:
            idtype = 'realname'
        return idtype

    @staticmethod
    def _build_search_kwargs(identifier, identifier_type):
        kwargs = {}

        filters = ['(objectclass=user)']
        if identifier_type == 'uid':
            filters.append('(uidnumber=%s)' % identifier)
        elif identifier_type == 'username':
            filters.append('(samaccountname=%s)' % identifier)
        elif identifier_type == 'realname':
            words = identifier.split()
            sn = words[-1]
            filters.append('(sn=%s)' % sn)
            if len(words) > 1:
                gn = ' '.join(words[:-1])
                filters.append('(givenname=%s)' % gn)
        elif identifier_type == 'mail':
            filters.append('(|(mail=%s)(proxyaddresses=smtp:%s))' % (identifier, identifier))
        # elif identifier_type in ['dn', 'distinguishedname']:
        #    kwargs['dn'] = identifier
        #    kwargs['scope'] = ldap.SCOPE_BASE
        else:
            filters.append('(%s=%s)' % (identifier_type, identifier))

        filterstr = '(&' + ''.join(filters) + ')'

        kwargs['filterstr'] = filterstr
        return kwargs


class LdapUser(LdapRecord, LdapUserIdentifierMixin):
    _DEFAULT_NAME_ATTRIBUTE = 'samaccountname'
    _DEFAULT_REPR_ATTRIBUTE = 'samaccountname'
    _PSEUDO_ATTRIBUTES = LdapRecord._PSEUDO_ATTRIBUTES + [
        PSEUDO_SYMBOL + 'uid',
        PSEUDO_SYMBOL + 'username',
        PSEUDO_SYMBOL + 'oe',
        PSEUDO_SYMBOL + 'phone',
        PSEUDO_SYMBOL + 'pem',
        PSEUDO_SYMBOL + 'realname',
        PSEUDO_SYMBOL + 'summary',
    ]

    def __getitem__(self, key):
        key = self.__keytransform__(key)
        if key == PSEUDO_SYMBOL + 'uid':
            real_key = 'uidnumber'
            try:
                values = self[real_key]
            except KeyError as exception:
                raise KeyError(key) from exception
        elif key == PSEUDO_SYMBOL + 'username':
            real_key = 'samaccountname'
            try:
                values = self[real_key]
            except KeyError as exception:
                raise KeyError(key) from exception
        elif key == PSEUDO_SYMBOL + 'oe':
            real_key = 'department'
            try:
                values = self[real_key]
            except KeyError as exception:
                raise KeyError(key) from exception
        elif key == PSEUDO_SYMBOL + 'phone':
            real_key = 'telephonenumber'
            try:
                values = self[real_key]
            except KeyError as exception:
                raise KeyError(key) from exception
        elif key == PSEUDO_SYMBOL + 'pem':
            real_key = 'usercertificate'
            try:
                certs = self[real_key]
            except KeyError as exception:
                raise KeyError(key) from exception
            values = [usercert2pem(cert) for cert in certs]
        elif key == PSEUDO_SYMBOL + 'realname':
            real_key = 'givenname'
            try:
                gn = self[real_key][0]
            except KeyError:
                logger.warning("User '%s' has no '%s' attribute", self.name(), real_key)
                gn = b'(?)'
            real_key = 'sn'
            try:
                sn = self[real_key][0]
            except KeyError:
                logger.warning("User '%s' has no '%s' attribute", self.name(), real_key)
                sn = b'(?)'
            values = [b'%b %b' % (gn, sn)]
        elif key == PSEUDO_SYMBOL + 'summary':
            un = self['samaccountname'][0]
            real_key = 'givenname'
            try:
                gn = self[real_key][0]
            except KeyError:
                logger.warning("User '%s' has no '%s' attribute", self.name(), real_key)
                gn = b'(?)'
            real_key = 'sn'
            try:
                sn = self[real_key][0]
            except KeyError:
                logger.warning("User '%s' has no '%s' attribute", self.name(), real_key)
                sn = b'(?)'
            values = [
                b'%b - %b %b' % (un, gn, sn)
            ]
            real_key = 'department'
            try:
                oe = self[real_key][0]
                values[0] += b' (%b)' % oe
            except KeyError:
                logger.warning("User '%s' has no '%s' attribute", self.name(), real_key)
            real_key = 'mail'
            try:
                mail = self[real_key][0]
                values[0] += b' <%b>' % mail
            except KeyError:
                logger.warning("User '%s' has no '%s' attribute", self.name(), real_key)
        elif key in ('badpasswordtime', 'lastlogon', 'lastlogontimestamp', 'pwdlastset'):
            tick_values = super().__getitem__(key)
            values = [winticks2fstr(int(v.decode('utf-8'))) for v in tick_values]
        else:
            values = super().__getitem__(key)
        return values

    def search(self, identifier, identifier_type='auto', **kwargs):
        if identifier_type == 'auto':
            identifier_type = self._identifier_type_autodetect(identifier)
            logger.info("Consider '%s' as %s", identifier, identifier_type)

        if identifier_type == 'username':
            self._default_repr_attribute = 'realname'

        search_kwargs = self._build_search_kwargs(identifier, identifier_type)
        return super().search(**search_kwargs)


class LdapUserSet(LdapRecordSet, LdapUserIdentifierMixin):
    RECORD_CLASS = LdapUser

    def search(self, identifier, identifier_type='auto', **kwargs):
        if identifier_type == 'auto':
            identifier_type = self._identifier_type_autodetect(identifier)
            logger.info("Consider '%s' as %s", identifier, identifier_type)

        if identifier_type == 'username':
            self._record_default_repr_attribute = 'realname'

        search_kwargs = self._build_search_kwargs(identifier, identifier_type)
        return super().search(**search_kwargs)


class LdapGroupIdentifierMixin:
    @staticmethod
    def _identifier_type_autodetect(identifier):
        if identifier.isdigit():
            idtype = 'gid'
        elif '*' in identifier:
            idtype = 'pattern'
        else:
            idtype = 'groupname'
        return idtype

    @staticmethod
    def _build_search_kwargs(identifier, identifier_type):
        kwargs = {}

        filters = ['(objectclass=group)']
        if identifier_type == 'gid':
            filters.append('(gidnumber=%s)' % identifier)
        elif identifier_type in ['groupname', 'pattern']:
            filters.append('(cn=%s)' % identifier)
        # elif identifier_type in ['dn', 'distinguishedname']:
        #    kwargs['dn'] = identifier
        #    kwargs['scope'] = ldap.SCOPE_BASE
        else:
            filters.append('(%s=%s)' % (identifier_type, identifier))

        filterstr = '(&' + ''.join(filters) + ')'

        kwargs['filterstr'] = filterstr
        return kwargs


class LdapGroup(LdapRecord, LdapGroupIdentifierMixin):
    _DEFAULT_NAME_ATTRIBUTE = 'cn'
    _DEFAULT_REPR_ATTRIBUTE = 'cn'

    def search(self, identifier, identifier_type='auto', **kwargs):
        if identifier_type == 'auto':
            identifier_type = self._identifier_type_autodetect(identifier)
            logger.info("Consider '%s' as %s", identifier, identifier_type)

        search_kwargs = self._build_search_kwargs(identifier, identifier_type)
        return super().search(**search_kwargs)


class LdapGroupSet(LdapRecordSet, LdapGroupIdentifierMixin):
    RECORD_CLASS = LdapGroup

    def search(self, identifier, identifier_type='auto', **kwargs):
        if identifier_type == 'auto':
            identifier_type = self._identifier_type_autodetect(identifier)
            logger.info("Consider '%s' as %s", identifier, identifier_type)

        search_kwargs = self._build_search_kwargs(identifier, identifier_type)
        return super().search(**search_kwargs)


class LdapHostIdentifierMixin:
    @staticmethod
    def _identifier_type_autodetect(identifier):
        if '.' in identifier:
            idtype = 'fqdn'
        elif OUSEP in identifier:
            idtype = 'ou'
        else:
            idtype = 'nodename'
        return idtype

    @staticmethod
    def _build_search_kwargs(identifier, identifier_type):
        kwargs = {}

        filters = ['(objectclass=computer)']
        if identifier_type == 'nodename':
            filters.append('(cn=%s)' % identifier)
        elif identifier_type == 'fqdn':
            filters.append('(dnshostname=%s)' % identifier)
        elif identifier_type == 'ou':
            kwargs['dn'] = LdapOu.ou2dn(identifier)
        # elif identifier_type in ['dn', 'distinguishedname']:
        #    kwargs['dn'] = identifier
        #    kwargs['scope'] = ldap.SCOPE_BASE
        else:
            if identifier_type.startswith(COMMENT_SYMBOL):
                key = identifier_type[len(COMMENT_SYMBOL):]
                expr1 = ''
            else:
                key = identifier_type
                expr1 = '(%s=%s)' % (key, identifier)
            expr2 = '(comment=*{sep}{key}: {val}{sep}*)'.format(sep=COMMENT_FIELDSEP, key=key, val=identifier)
            if expr1:
                expr = '(|' + expr1 + expr2 + ')'
            else:
                expr = expr2
            filters.append(expr)

        filterstr = '(&' + ''.join(filters) + ')'

        kwargs['filterstr'] = filterstr
        return kwargs


class LdapHost(LdapRecord, LdapHostIdentifierMixin):
    _DEFAULT_NAME_ATTRIBUTE = 'dnshostname'
    _DEFAULT_REPR_ATTRIBUTE = 'dnshostname'
    _PSEUDO_ATTRIBUTES = LdapRecord._PSEUDO_ATTRIBUTES + [
        PSEUDO_SYMBOL + 'fqdn',
        PSEUDO_SYMBOL + 'nodename',
        PSEUDO_SYMBOL + 'ou',
    ]

    def __getitem__(self, key):
        key = self.__keytransform__(key)
        parent_getter = super().__getitem__
        if key == 'dnshostname':
            try:
                values = parent_getter(key)
            except KeyError:
                values = parent_getter('cn')
                logger.warning("Host '%s' has no '%s' attribute", values[0].decode(), key)
        elif key.startswith(COMMENT_SYMBOL):
            comment_dict = self._comment_dict()
            real_key = key[len(COMMENT_SYMBOL):]
            try:
                values = [comment_dict[real_key]]
            except KeyError as exception:
                raise KeyError(key) from exception
        elif key == PSEUDO_SYMBOL + 'fqdn':
            try:
                real_key = 'dnshostname'
                values = self[real_key]
            except KeyError as exception:
                raise KeyError(key) from exception
        elif key == PSEUDO_SYMBOL + 'nodename':
            try:
                real_key = 'cn'
                values = [cn.lower() for cn in self[real_key]]
            except KeyError as exception:
                raise KeyError(key) from exception
        elif key == PSEUDO_SYMBOL + 'ou':
            values = []
            for dn in self['distinguishedname']:
                rdns = ldap.dn.str2dn(dn)
                ou_rdns = rdns[1:]
                ou_dn = ldap.dn.dn2str(ou_rdns)
                values.append(OUSEP + LdapOu.dn2ou(ou_dn))
        else:
            values = parent_getter(key)
        return values

    def __keytransform__(self, key):
        key = super().__keytransform__(key)
        comment_key = COMMENT_SYMBOL + key
        if comment_key in self.store:
            key = comment_key
        return key

    def _comment_dict(self):
        return self._comment2dict(self['comment'][0].decode())

    @staticmethod
    def _comment2dict(comment):
        comment = comment.strip(COMMENT_FIELDSEP)
        d = {}
        for s in comment.split(COMMENT_FIELDSEP):
            if not s:
                continue
            k, v = s.split(': ', 1)
            d[k] = v
        return d

    @staticmethod
    def _dict2comment(d):
        if not d:
            return ''
        parts = ['%s: %s' % (k, v) for k, v in d.items()]
        return COMMENT_FIELDSEP + COMMENT_FIELDSEP.join(parts) + COMMENT_FIELDSEP

    def search(self, identifier, identifier_type='auto', **kwargs):
        if identifier_type == 'auto':
            identifier_type = self._identifier_type_autodetect(identifier)
            logger.info("Consider '%s' as %s", identifier, identifier_type)

        search_kwargs = self._build_search_kwargs(identifier, identifier_type)
        return super().search(**search_kwargs)

    def load(self, data):
        super().load(data)
        try:
            comment_dict = self._comment_dict()
            for k in comment_dict:
                pseudo_key = COMMENT_SYMBOL + k
                self[pseudo_key] = []
        except KeyError:
            pass

    def modify_comment(self, field, value=None):
        if not ENABLE_HOST_COMMENT_MODIFY:
            raise Exception('Feature not enabled')
        if field.startswith(COMMENT_SYMBOL):
            field = field[len(COMMENT_SYMBOL):]
        try:
            comment = self['comment'][0].decode()
            operation = ldap.MOD_REPLACE  # pylint: disable=no-member
        except KeyError:
            comment = ''
            operation = ldap.MOD_ADD  # pylint: disable=no-member
        comment_dict = self._comment2dict(comment)
        if value is None:
            if field in comment_dict:
                del comment_dict[field]
            else:
                logger.warning("Comment field '%s' is already empty", field)
        else:
            comment_dict[field] = value
        comment = self._dict2comment(comment_dict)
        if not comment:
            if operation == ldap.MOD_ADD:  # pylint: disable=no-member
                logger.info('Nothing to do')
                return None
            logger.info('Deleting empty comment')
            operation = ldap.MOD_DELETE  # pylint: disable=no-member
            comment = None

        if comment:
            value_bytes = comment.encode()
        else:
            value_bytes = None

        modlist = [
            (operation, 'comment', value_bytes),
        ]
        return self.modify(modlist)


class LdapHostSet(LdapRecordSet, LdapHostIdentifierMixin):
    RECORD_CLASS = LdapHost

    def search(self, identifier, identifier_type='auto', **kwargs):
        if identifier_type == 'auto':
            identifier_type = self._identifier_type_autodetect(identifier)
            logger.info("Consider '%s' as %s", identifier, identifier_type)

        search_kwargs = self._build_search_kwargs(identifier, identifier_type)
        return super().search(**search_kwargs)


class LdapOu(LdapRecord):
    AD_BASE_DN = AD_BASE_DN
    BASE_OU = DEFAULT_HOST_OU

    def __str__(self):
        return self.absname()

    def search(self, identifier, **kwargs):
        dn = self.__class__.ou2dn(identifier)
        filterstr = '(objectclass=*)'
        return super().search(dn, ldap.SCOPE_BASE, filterstr)  # pylint: disable=no-member

    def subous(self):
        subs = []
        dn = self['distinguishedname'][0].decode()
        filterstr = '(objectclass=organizationalunit)'
        self._ldap_api.bind()
        results = self._ldap_api.search(dn, ldap.SCOPE_ONELEVEL, filterstr)  # pylint: disable=no-member
        for tup in results:
            dn = tup[0]
            if dn:
                data = tup[1]
                sub_record = self.__class__(ldap_api=self._ldap_api)
                sub_record.load(data)
                subs.append(sub_record)
        return subs

    def name(self, base_ou=None):  # pylint: disable=arguments-renamed
        if base_ou is None:
            base_ou = self.BASE_OU
        base_ou = base_ou.rstrip(OUSEP)
        base_ou += OUSEP
        dn = self['distinguishedname'][0].decode()
        ou = OUSEP + self.__class__.dn2ou(dn)
        if ou.startswith(base_ou):
            ou = ou[len(base_ou):]
        return ou

    def absname(self):
        return OUSEP + self.name(OUSEP)

    @classmethod
    def dn2ou(cls, dn):
        base_dn = cls.AD_BASE_DN
        if dn.lower().endswith(base_dn.lower()):
            dn = dn[:0 - (len(DNSEP) + len(base_dn))]
        if not dn:
            return ''
        rdns = ldap.dn.str2dn(dn)
        parts = [rdn[0][1] for rdn in rdns]
        parts.reverse()
        return OUSEP.join(parts)

    @classmethod
    def ou2dn(cls, ou):
        base_dn = cls.AD_BASE_DN
        base_ou = cls.BASE_OU
        if ou == OUSEP:
            return base_dn
        if not ou.startswith(OUSEP):
            ou = OUSEP.join([base_ou, ou])
        ou = ou.strip(OUSEP)
        names = ou.split(OUSEP)
        names.reverse()
        parts = ['OU=%s' % name for name in names]
        parts.append(base_dn)
        return DNSEP.join(parts)


class SubCommand:
    RECORD_DIVIDER = RECORD_DIVIDER
    RECORD_CLASS = LdapRecordSet

    def __init__(self):
        self.cmd_args = None
        self.ldap_api = None

    def __call__(self, cmd_args, ldap_api):
        self.cmd_args = cmd_args
        self.ldap_api = ldap_api
        return self._action()

    def _list_records_with_attributes(self, identifier, identifier_type, attributes, prefixes=None, only_one=False):
        set_class = self.RECORD_CLASS
        recordset = set_class(ldap_api=self.ldap_api)
        n = recordset.search(identifier, identifier_type)
        if n < 1:
            logger.error('No %s found', set_class.class_typename(singular=True, capitalize=False))
            return posix.EX_NOUSER
        if n > 1:
            if only_one:
                logger.error("'%s' is ambiguous", identifier)
                return posix.EX_DATAERR
            logger.warning('Found %d %s', n, set_class.class_typename(capitalize=False))

        formatstr = '{value}'
        if prefixes:
            prefixes.reverse()
            for prefix in prefixes:
                formatstr = ('{%s}: ' % prefix) + formatstr

        try:
            tuples = recordset.list(attributes)
        except KeyError as exception:
            logger.error('%s has no attribute %s', set_class.class_typename(singular=True), exception)
            return posix.EX_NOINPUT

        record_divider = os.linesep
        for record_tup in tuples:
            n = len(record_tup[2])
            if n > 1:
                record_divider = os.linesep + self.RECORD_DIVIDER + os.linesep
                if not prefixes:
                    formatstr = '{attribute}: ' + formatstr
                break

        if prefixes is None and len(tuples) > 1 and logger.getEffectiveLevel() < logging.ERROR:
            formatstr = '{name}: ' + formatstr

        keys = []
        divs = {}
        for record_tup in tuples:
            values = []
            name = record_tup[1]
            for attr_tup in record_tup[2]:
                attr = attr_tup[0]
                byte_value = attr_tup[1]
                try:
                    value = byte_value.decode()
                except AttributeError:
                    value = byte_value
                except UnicodeDecodeError:
                    value = 'BASE64:' + base64.b64encode(byte_value).decode()
                s = formatstr.format(name=name, attribute=attr, value=value)
                values.append(s)
            if values:
                values.sort()
                keys.append(name)
                divs[name] = os.linesep.join(values)

        keys.sort()
        sorted_divs = [divs[key] for key in keys]
        output = record_divider.join(sorted_divs)
        if output:
            output += os.linesep
        sys.stdout.write(output)
        return posix.EX_OK

    def _show_record(self, identifier, identifier_type, attributes, prefixes):
        return self._list_records_with_attributes(identifier, identifier_type, attributes, prefixes, only_one=True)

    def _action(self):
        raise NotImplementedError()

    @property
    def name(self):
        return self.__class__.__name__[:0 - len('Command')].lower()

    @property
    def help(self):
        return 'stuff about %s' % self.name

    def setup_argparser(self, parser):
        pass


class UsersCommand(SubCommand):
    RECORD_CLASS = LdapUserSet

    def setup_argparser(self, parser):
        default_action = 'list-users'
        group = parser.add_argument_group('action options',
                                          'the default action is \'%s\'' % default_action)
        action_group = group.add_mutually_exclusive_group()
        action_group.add_argument('-l', '--list-users',
                                  action='store_const', dest='action', const='list-users',
                                  help='show attributes of matching users')
        action_group.add_argument('-s', '--show-user',
                                  action='store_const', dest='action', const='show-user',
                                  help='show attributes of a single user')
        action_group.add_argument('-g', '--list-groups',
                                  action='store_const', dest='action', const='list-groups',
                                  help='list groups, where USER is in')
        parser.set_defaults(action=default_action)

        attr_group = parser.add_argument_group('output options',
                                               'by default the output is unexpected')
        attr_group.add_argument('-o', metavar='ATTR[,ATTR...]',
                                action='append', dest='attrstrlst',
                                help='list of attributes seperated by comma, can be used multiple times')
        attr_group.add_argument('-u', '--username',
                                action='append_const', dest='attrlst', const='username',
                                help='shortcut for -o username')
        attr_group.add_argument('-n', '--realname',
                                action='append_const', dest='attrlst', const='realname',
                                help='shortcut for -o realname')
        attr_group.add_argument('-a', '--all',
                                action='append_const', dest='attrlst', const='all',
                                help='shortcut for -o all')

        attr_group.add_argument('-i', '--pre-username',
                                action='append_const', dest='output_prefixes', const='name',
                                help='prefix values with the username')
        attr_group.add_argument('-k', '--pre-attribute',
                                action='append_const', dest='output_prefixes', const='attribute',
                                help='prefix values with the attribute name')

        parser.add_argument('-r', '--recursive',
                            action='store_true', dest='recursive',
                            help='group membership will be resolved recursively')

        group = parser.add_argument_group('input options',
                                          'by default the input type is autodetected')
        inputspec_group = group.add_mutually_exclusive_group()
        inputspec_group.add_argument('-R', '--is-realname',
                                     action='store_const', dest='identifier_type', const='realname',
                                     help='interpret USER as realname')
        inputspec_group.add_argument('-U', '--is-username',
                                     action='store_const', dest='identifier_type', const='username',
                                     help='interpret USER as username')
        inputspec_group.add_argument('-I', '--is-uid',
                                     action='store_const', dest='identifier_type', const='uid',
                                     help='interpret USER as uid')
        inputspec_group.add_argument('-M', '--is-mail',
                                     action='store_const', dest='identifier_type', const='mail',
                                     help='interpret USER as mail address')
        inputspec_group.add_argument('-A', '--is-attribute', metavar='ATTR',
                                     action='store', dest='identifier_type',
                                     help='interpret USER as attribute ATTR')
        parser.set_defaults(identifier_type='auto')

        parser.add_argument('identifier', metavar='USER',
                            help='a realname, username, mail address or numeric user id - (* can be used for patterns)')
        parser.add_argument('identifier_ext', metavar='...', nargs='*',
                            help='additional parts of the realname')

    def _get_parents(self, record, recursive=False):
        try:
            parent_dns = record['memberof']
        except KeyError:
            return []

        groups = []
        for dn_enc in parent_dns:
            dn = dn_enc.decode()
            parent = LdapGroup(ldap_api=self.ldap_api)
            try:
                parent.search(dn, 'distinguishedname')
            except LdapGroup.NotFoundException:
                logger.warning("Group DN '%s' is listed but cannot be found", dn)
                continue
            except LdapGroup.NotUniqueException:
                logger.warning("Group DN '%s' is not unique", dn)
                continue
            gn = parent.name()
            if gn not in groups:
                groups.append(gn)
            if recursive:
                ancestors = self._get_parents(parent, recursive)
                groups.extend(set(ancestors) - set(groups))
        return groups

    def _list_groups(self, identifier, identifier_type, recursive=False):
        user = LdapUser(ldap_api=self.ldap_api)
        try:
            user.search(identifier, identifier_type)
        except LdapUser.NotFoundException:
            logger.error("User not found")
            return posix.EX_NOUSER
        except LdapUser.NotUniqueException:
            logger.error("'%s' is ambiguous", identifier)
            return posix.EX_DATAERR

        groups = self._get_parents(user, recursive)
        groups.sort()
        output = os.linesep.join(groups)
        if output:
            output += os.linesep
        sys.stdout.write(output)
        return posix.EX_OK

    def _list_records_with_attributes(self, identifier, identifier_type, attributes, prefixes=None, only_one=False):
        if not attributes and logger.getEffectiveLevel() < logging.ERROR:
            attributes = 'summary'
        return super()._list_records_with_attributes(identifier, identifier_type, attributes,
                                                     prefixes=prefixes, only_one=only_one)

    def _action(self):
        action = self.cmd_args.action

        identifier = ' '.join([self.cmd_args.identifier] + self.cmd_args.identifier_ext)
        identifier_type = self.cmd_args.identifier_type

        attributes = []
        if self.cmd_args.attrstrlst:
            for attrstr in self.cmd_args.attrstrlst:
                attrstr.strip(',')
                attributes.extend(attrstr.split(','))
        if self.cmd_args.attrlst:
            attributes.extend(self.cmd_args.attrlst)

        prefixes = self.cmd_args.output_prefixes

        if action == 'list-users':
            return self._list_records_with_attributes(identifier, identifier_type, attributes, prefixes=prefixes)
        if action == 'show-user':
            return self._show_record(identifier, identifier_type, attributes, prefixes=prefixes)
        if action == 'list-groups':
            return self._list_groups(identifier, identifier_type, recursive=self.cmd_args.recursive)
        logger.error("What? Code on fire!")
        return posix.EX_SOFTWARE


class GroupsCommand(SubCommand):
    def setup_argparser(self, parser):
        default_action = 'list-groups'
        group = parser.add_argument_group('action options',
                                          'the default action is \'%s\'' % default_action)
        action_group = group.add_mutually_exclusive_group()
        action_group.add_argument('-l', '--list-groups',
                                  action='store_const', dest='action', const='list-groups',
                                  help='list matching group names (GROUP will be taken as pattern, use * as wildcard)')
        action_group.add_argument('-g', '--groups',
                                  action='store_const', dest='action', const='groups',
                                  help='list groups that are member of GROUP')
        action_group.add_argument('-u', '--users',
                                  action='store_const', dest='action', const='users',
                                  help='list users that are member of GROUP')
        action_group.add_argument('-m', '--members',
                                  action='store_const', dest='action', const='members',
                                  help='list users and groups that are member of GROUP')
        action_group.add_argument('-p', '--parents',
                                  action='store_const', dest='action', const='parents',
                                  help='list groups where GROUP is a member')
        parser.set_defaults(action=default_action)

        parser.add_argument('-r', '--recursive',
                            action='store_true', dest='recursive',
                            help='group membership will be resolved recursively')

        parser.add_argument('identifier', metavar='GROUP', nargs='?', default=DEFAULT_GROUP_PATTERN,
                            help='a group name, group pattern or numeric group id'
                                 ' - default: %s' % DEFAULT_GROUP_PATTERN)

    def _list_groups(self, pattern=None):
        groupset = LdapGroupSet(ldap_api=self.ldap_api)
        try:
            n = groupset.search(pattern, identifier_type='pattern')
        except ldap.SIZELIMIT_EXCEEDED:  # pylint: disable=no-member
            logger.error("LDAP size limit exceeded: result set to large")
            return posix.EX_PROTOCOL

        if n < 1:
            logger.error("No groups found")
            return posix.EX_NOUSER

        groups = groupset.names()
        groups.sort()
        output = os.linesep.join(groups) + os.linesep
        sys.stdout.write(output)
        return posix.EX_OK

    def _get_parents(self, group, recursive=False):
        try:
            parent_dns = group['memberof']
        except KeyError:
            return []

        groups = []
        for dn_enc in parent_dns:
            dn = dn_enc.decode()
            parent = LdapGroup(ldap_api=self.ldap_api)
            try:
                parent.search(dn, 'distinguishedname')
            except LdapGroup.NotFoundException:
                logger.warning("Group DN '%s' is listed as parent of '%s' but cannot be found", dn, group)
                continue
            except LdapGroup.NotUniqueException:
                logger.warning("Group DN '%s' is not unique", dn)
                continue
            gn = parent.name()
            if gn not in groups:
                groups.append(gn)
            if recursive:
                ancestors = self._get_parents(parent, recursive)
                groups.extend(set(ancestors) - set(groups))
        return groups

    def _list_parents(self, identifier, recursive=False):
        group = LdapGroup(ldap_api=self.ldap_api)
        try:
            group.search(identifier, 'groupname')
        except LdapGroup.NotFoundException:
            logger.error("Group not found")
            return posix.EX_NOUSER
        except LdapGroup.NotUniqueException:
            logger.error("'%s' is ambiguous", identifier)
            return posix.EX_DATAERR

        groups = self._get_parents(group, recursive)
        groups.sort()
        output = os.linesep.join(groups)
        if output:
            output += os.linesep
        sys.stdout.write(output)
        return posix.EX_OK

    def _get_members(self, group, list_groups=True, list_users=False, recursive=False):
        dn = group['distinguishedname'][0].decode()

        groups = []
        users = []
        if recursive:
            groupset = LdapGroupSet(ldap_api=self.ldap_api)
            groupset.search(dn, 'memberof')
            for member_group in groupset:
                member_groups, member_users = self._get_members(groupset[member_group], list_groups, list_users,
                                                                recursive)
                if list_groups:
                    gn = groupset[member_group].name()
                    if gn not in groups:
                        groups.append(gn)
                    groups.extend(set(member_groups) - set(groups))
                    for gn in member_groups:
                        if gn not in groups:
                            groups.append(gn)
                if list_users:
                    users.extend(set(member_users) - set(users))
                    for un in member_users:
                        if un not in users:
                            users.append(un)
        elif list_groups:
            groupset = LdapGroupSet(ldap_api=self.ldap_api)
            groupset.search(dn, 'memberof')
            groups.extend(groupset.names())

        if list_users:
            userset = LdapUserSet(ldap_api=self.ldap_api)
            userset.search(dn, 'memberof')
            users.extend(userset.names())

        return groups, users

    def _list_members(self, identifier, list_groups=True, list_users=False, recursive=False):
        group = LdapGroup(ldap_api=self.ldap_api)
        try:
            group.search(identifier, 'groupname')
        except LdapGroup.NotFoundException:
            logger.error("Group not found")
            return posix.EX_NOUSER
        except LdapGroup.NotUniqueException:
            logger.error("'%s' is ambiguous", identifier)
            return posix.EX_DATAERR

        groups, users = self._get_members(group, list_groups, list_users, recursive)
        lines = []

        if list_groups and list_users:
            group_formatstr = 'Group: {group}'
            user_formatstr = 'User: {user}'
        else:
            group_formatstr = '{group}'
            user_formatstr = '{user}'

        if list_groups:
            groups.sort()
            lines.extend(group_formatstr.format(group=n) for n in groups)
        if list_users:
            users.sort()
            if logger.getEffectiveLevel() < logging.ERROR:
                for un in users:
                    user = LdapUser(ldap_api=self.ldap_api)
                    try:
                        user.search(un, 'username')
                    except LdapUser.NotFoundException:
                        logger.warning("User '%s' is listed as member but cannot be found", un)
                        continue
                    except LdapUser.NotUniqueException:
                        logger.warning("User '%s' is member but is not unique", un)
                        continue
                    text = user['summary'][0].decode()
                    lines.append(user_formatstr.format(user=text))
            else:
                lines.extend(user_formatstr.format(user=n) for n in users)

        output = os.linesep.join(lines)
        if output:
            output += os.linesep
        sys.stdout.write(output)
        return posix.EX_OK

    def _action(self):
        action = self.cmd_args.action
        identifier = self.cmd_args.identifier
        recursive = self.cmd_args.recursive

        if action == 'list-groups':
            return self._list_groups(identifier)
        if action == 'groups':
            return self._list_members(identifier, list_groups=True, list_users=False, recursive=recursive)
        if action == 'users':
            return self._list_members(identifier, list_groups=False, list_users=True, recursive=recursive)
        if action == 'members':
            return self._list_members(identifier, list_groups=True, list_users=True, recursive=recursive)
        if action == 'parents':
            return self._list_parents(identifier, recursive=recursive)
        logger.error("What? Code on fire!")
        return posix.EX_SOFTWARE


class HostsCommand(SubCommand):
    RECORD_CLASS = LdapHostSet

    def setup_argparser(self, parser):
        default_action = 'list-hosts'
        group = parser.add_argument_group('action options',
                                          'the default action is \'%s\'' % default_action)
        action_group = group.add_mutually_exclusive_group()
        action_group.add_argument('-l', '--list-hosts',
                                  action='store_const', dest='action', const='list-hosts',
                                  help='show attributes of hosts')
        action_group.add_argument('-s', '--show-host',
                                  action='store_const', dest='action', const='show-host',
                                  help='show attributes of a single host')
        if ENABLE_HOST_COMMENT_MODIFY:
            action_group.add_argument('-m', '--modify-comment',
                                      action='store_const', dest='action', const='modify-comment',
                                      help='modify comment attribute of a single host')
        action_group.add_argument('-u', '--list-ous',
                                  action='store_const', dest='action', const='list-ous',
                                  help='list OUs (HOST will be taken as base OU)')
        parser.set_defaults(action=default_action)

        parser.add_argument('-r', '--recursive',
                            action='store_true', dest='recursive',
                            help='OUs will be resolved recursively')

        attr_group = parser.add_argument_group('output options',
                                               'by default the output is unexpected')
        attr_group.add_argument('-o', metavar='ATTR[,ATTR,...]',
                                action='append', dest='attrstrlst',
                                help='list of attributes seperated by comma, can be used multiple times')

        attr_group.add_argument('-i', '--pre-hostname',
                                action='append_const', dest='output_prefixes', const='name',
                                help='prefix values with the username')
        attr_group.add_argument('-k', '--pre-attribute',
                                action='append_const', dest='output_prefixes', const='attribute',
                                help='prefix values with the attribute name')

        group = parser.add_argument_group('input options',
                                          'by default the argument type is autodetected')
        inputspec_group = group.add_mutually_exclusive_group()
        inputspec_group.add_argument('-H', '--fqdn',
                                     action='store_const', dest='identifier_type', const='fqdn',
                                     help='interpret HOST as fully qualified DNS name')
        inputspec_group.add_argument('-N', '--nodename',
                                     action='store_const', dest='identifier_type', const='nodename',
                                     help='interpret HOST as node name (short host name)')
        inputspec_group.add_argument('-O', '--ou',
                                     action='store_const', dest='identifier_type', const='ou',
                                     help='interpret HOST as OU')
        inputspec_group.add_argument('-A', '--is-attribute', metavar='ATTR',
                                     action='store', dest='identifier_type',
                                     help='interpret HOST as host attribute ATTR')
        parser.set_defaults(identifier_type='auto')

        parser.add_argument('identifier', metavar='HOST', nargs='?', default=DEFAULT_HOST_OU,
                            help='a FQDN, node name (short hostname) or OU - default: %s' % DEFAULT_HOST_OU)

        if ENABLE_HOST_COMMENT_MODIFY:
            parser.add_argument('modify_field', metavar='FIELD', nargs='?', default=None,
                                help='only in modify-comment mode: field name to modify')
            parser.add_argument('new_value', metavar='VALUE', nargs='?', default=None,
                                help='only in modify-comment mode: new value for FIELD'
                                     ' - if omitted, FIELD will be deleted')
            parser.add_argument('new_value_ext', metavar='...', nargs='*',
                                help='more value')

    def _get_subous(self, ou, base_ou=None, recursive=False):
        subs = []
        for sub in ou.subous():
            subname = sub.name(base_ou)
            subs.append(subname)
            if recursive:
                subsubs = self._get_subous(sub, base_ou, recursive)
                if subsubs:
                    subs.extend(subsubs)
        return subs

    def _list_ous(self, identifier=None, recursive=False):
        ou = LdapOu(ldap_api=self.ldap_api)
        try:
            ou.search(identifier)
        except LdapOu.NotFoundException:
            logger.error("OU not found")
            return posix.EX_NOUSER
        except LdapOu.NotUniqueException:
            logger.error("'%s' is ambiguous", identifier)
            return posix.EX_DATAERR

        base_ou = ou.absname()
        subs = self._get_subous(ou, base_ou, recursive)
        subs.sort()
        output = os.linesep.join(subs)
        if output:
            output += os.linesep
        sys.stdout.write(output)
        return posix.EX_OK

    def _modify_comment(self, identifier, identifier_type, field, value=None):
        host = LdapHost(ldap_api=self.ldap_api)
        try:
            host.search(identifier, identifier_type)
        except LdapHost.NotFoundException:
            logger.error("Host not found")
            return posix.EX_NOUSER
        except LdapHost.NotUniqueException:
            logger.error("'%s' is ambiguous", identifier)
            return posix.EX_DATAERR

        host.modify_comment(field, value)
        return posix.EX_OK

    def _action(self):
        action = self.cmd_args.action

        identifier = self.cmd_args.identifier
        identifier_type = self.cmd_args.identifier_type

        attributes = []
        if self.cmd_args.attrstrlst:
            for attrstr in self.cmd_args.attrstrlst:
                attrstr.strip(',')
                attributes.extend(attrstr.split(','))

        if self.cmd_args.output_prefixes:
            prefixes = self.cmd_args.output_prefixes
        else:
            prefixes = []

        recursive = self.cmd_args.recursive

        if ENABLE_HOST_COMMENT_MODIFY:
            modify_field = self.cmd_args.modify_field
            new_value = self.cmd_args.new_value
            if new_value and self.cmd_args.new_value_ext:
                new_value += ' ' + ' '.join(self.cmd_args.new_value_ext)
        else:
            modify_field = None
            new_value = None

        if action == 'list-hosts':
            return self._list_records_with_attributes(identifier, identifier_type, attributes, prefixes=prefixes)
        if action == 'show-host':
            return self._show_record(identifier, identifier_type, attributes, prefixes=prefixes)
        if action == 'modify-comment':
            return self._modify_comment(identifier, identifier_type, modify_field, new_value)
        if action == 'list-ous':
            return self._list_ous(identifier, recursive)
        logger.error("What? Code on fire!")
        return posix.EX_SOFTWARE


class Program:
    subcmd_classes = [UsersCommand, GroupsCommand, HostsCommand]

    def __init__(self):
        argparser = self._setup_argparser()
        subargparsers = argparser.add_subparsers(dest='subcmd', metavar='CMD',
                                                 title='subcommands',
                                                 description="Use '%(prog)s CMD -h' to show help for a subcommand")
        subcmds = {}
        for subcmd_class in self.subcmd_classes:
            subcmd = subcmd_class()
            subargparser = subargparsers.add_parser(subcmd.name, help=subcmd.help)
            subcmd.setup_argparser(subargparser)
            subcmds[subcmd.name] = subcmd
        self._argparser = argparser
        self._subcmds = subcmds

    @staticmethod
    def _setup_argparser():
        kwargs = {
            'description': PROGRAM_DESCRIPTION,
            'epilog': 'More information: {url}'.format(url=PROGRAM_URL),
        }
        parser = argparse.ArgumentParser(**kwargs)

        parser.add_argument('-V', '--version', action='version',
                            version='%(prog)s ' + PROGRAM_VERSION)

        group = parser.add_argument_group()
        group.add_argument('--debug',
                           action='store_const', dest='verbose_level', const='debug',
                           help='enable debug output (level DEBUG)')
        group.add_argument('-v', '--verbose',
                           action='store_const', dest='verbose_level', const='info',
                           help='be more verbose (level INFO)')
        group.add_argument('-q', '--quiet',
                           action='store_const', dest='verbose_level', const='error',
                           help='be less verbose (level ERROR)')
        parser.set_defaults(verbose_level='warning')

        group = parser.add_argument_group('ldap options')
        group.add_argument('-x',
                           action='store_true', dest='simple_bind',
                           help='Use simple authentication instead of SASL/GSSAPI')
        group.add_argument('-D', metavar='binddn',
                           dest='binddn', default=DEFAULT_SIMPLE_BIND_DN,
                           help='Use the Distinguished Name binddn to bind to the LDAP directory'
                                ' - default: %s' % DEFAULT_SIMPLE_BIND_DN)
        group.add_argument('-H', metavar='ldapuri',
                           dest='ldapuri', default=DEFAULT_LDAP_URI,
                           help='Specify URI referring  to  the ldap server - default: %s' % DEFAULT_LDAP_URI)

        return parser

    def _parse_args(self, argv=None):
        if argv is None:
            argv = sys.argv[1:]
        if not argv:
            argv = ['--help']

        return self._argparser.parse_args(argv)

    @staticmethod
    def _get_ldap_credentials(binddn):
        prompt = PASSWORD_PROMPT.format(binddn=binddn)
        return getpass.getpass(prompt)

    def _setup_ldap_api(self, cmd_args):
        ldap_kwargs = {
            'uri': cmd_args.ldapuri,
            'simple_bind': cmd_args.simple_bind
        }
        if cmd_args.simple_bind:
            if not cmd_args.binddn:
                raise ldap.INVALID_CREDENTIALS('No bind DN given')  # pylint: disable=no-member
            ldap_kwargs['binddn'] = cmd_args.binddn
            ldap_kwargs['credential'] = self._get_ldap_credentials(cmd_args.binddn)
        return LdapApi(**ldap_kwargs)

    def __call__(self, *args, **kwargs):
        argv = kwargs.get('argv', None)
        cmd_args = self._parse_args(argv)

        verbose_level_str = cmd_args.verbose_level.upper()
        verbose_level_val = getattr(logging, verbose_level_str)
        logger.setLevel(verbose_level_val)

        try:
            ldap_api = self._setup_ldap_api(cmd_args)
            exitval = self._subcmds[cmd_args.subcmd](cmd_args, ldap_api)
        except ldap.INVALID_CREDENTIALS as exception:  # pylint: disable=no-member
            logger.error('LDAP Error: %s', exception)
            exitval = posix.EX_NOPERM
        except ldap.LOCAL_ERROR as exception:  # pylint: disable=no-member
            logger.error('LDAP Error: %s', exception)
            exitval = posix.EX_SOFTWARE
        except ldap.OPERATIONS_ERROR as exception:  # pylint: disable=no-member
            logger.error('LDAP Error: %s', exception)
            exitval = posix.EX_SOFTWARE
        except ldap.SERVER_DOWN as exception:  # pylint: disable=no-member
            logger.error('%s', exception)
            exitval = posix.EX_UNAVAILABLE
        except Exception as exception:
            if verbose_level_val <= logging.DEBUG:
                raise exception
            logger.error('%s: %s', exception.__class__.__name__, exception)
            exitval = posix.EX_SOFTWARE
        return exitval


def main(*args, **kwargs):
    program = Program()
    exitval = program(*args, **kwargs)
    sys.exit(exitval)


if __name__ == '__main__':
    main()
